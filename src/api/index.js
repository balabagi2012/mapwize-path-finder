import axios from "axios";
import { API_KEY, API_BASE } from "../config"
export default class Api {
  static async getRoutegraphs(floor = 2) {
    try {
      const response = await axios.get(`${API_BASE}/routegraphs?api_key=${API_KEY}&floor=${floor}`)
      const responseData = response.data
      return responseData;
    } catch (error) {
      console.log(error)
    }
  }

  static async getPlaces(floor = 2) {
    try {
      const response = await axios.get(`${API_BASE}/places?api_key=${API_KEY}&floor=${floor}`)
      const responseData = response.data
      return responseData;
    } catch (error) {
      console.log(error)
    }
  }
}