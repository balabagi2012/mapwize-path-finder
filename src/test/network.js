import PathFinder from "geojson-path-finder";
import geojson from "../mock/routeGraph-geoJson"
const pathFinder = new PathFinder(geojson, { precision: 1e-3 });
const start = {
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [22.427086623502603, 114.20963847919921]
  },
  "properties": {
    "name": "5d37145814fdbf0016e1b1f0"
  }
}

const end = {
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [22.427040411587818, 114.20915618536748]
  },
  "properties": {
    "name": "5d371374a3852a0016cba849"
  }
}
console.log(pathFinder.findPath(start, end))