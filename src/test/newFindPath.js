import fs from "fs";
import {
  Graph,
  CoordinateLookup,
  buildGeoJsonPath,
  buildEdgeIdList
} from "geojson-dijkstra";
import placesJson from "../mock/places-mapwize.json";
import geoJsonData from "../mock/routeGraph-geoJson.json";
export default findPath = (fromPlaceId, toPlace) => {
  const geojson = JSON.parse(geoJsonData);
  const graph = new Graph(geojson);
  const lookup = new CoordinateLookup(graph);
  const finder = graph.createFinder({
    parseOutputFns: [buildGeoJsonPath, buildEdgeIdList]
  });

  const fromPlace = placesJson.places.find(place => place._id === fromPlaceId);
  const toPlace = placesJson.places.find(place => place._id === toPlaceId);
  const fromPlaceEntrance = fromPlace.entrance
    ? fromPlace.entrance
    : fromPlace.marker;
  const toPlaceEntrance = toPlace.entrance ? toPlace.entrance : toPlace.marker;
  const from = [fromPlaceEntrance.latitude, fromPlaceEntrance.longitude];
  const to = [toPlaceEntrance.latitude, toPlaceEntrance.longitude];
  const closestFrom = lookup.getClosestNetworkPt(from[0], from[1]);
  const closestTo = lookup.getClosestNetworkPt(to[0], to[1]);

  const result = finder.findPath(closestFrom, closestTo);
  const paths = result.geojsonPath.features.map(feature => {
    return feature.geometry.coordinates;
  });

  let finalPath = [from, closestFrom];
  paths.forEach(path => {
    finalPath.push(path[0]);
    finalPath.push(path[1]);
  });
  finalPath.push(closestTo);
  finalPath.push(to);
  return finalPath;
};
