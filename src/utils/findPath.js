import fs from "fs";
import {
  Graph,
  CoordinateLookup,
  buildGeoJsonPath,
  buildEdgeIdList
} from "geojson-dijkstra";
import placesJson from "../mock/places-mapwize.json";
import geoJsonData from "../mock/routeGraph-geoJson.json";
export default function (from, to) {
  const graph = new Graph(geoJsonData);
  const lookup = new CoordinateLookup(graph);
  const finder = graph.createFinder({
    parseOutputFns: [buildGeoJsonPath, buildEdgeIdList]
  });
  let fromLocation = [], toLocation = [];
  if (Array.isArray(from)) {
    fromLocation = from
  } else {
    const fromPlace = placesJson.places.find(place => place._id === from);
    const fromPlaceEntrance = fromPlace.entrance
      ? fromPlace.entrance
      : fromPlace.marker;
    fromLocation = [fromPlaceEntrance.latitude, fromPlaceEntrance.longitude]
  }
  if (Array.isArray(to)) {
    toLocation = to
  } else {
    const toPlace = placesJson.places.find(place => place._id === to);
    const toPlaceEntrance = toPlace.entrance ? toPlace.entrance : toPlace.marker;
    toLocation = [toPlaceEntrance.latitude, toPlaceEntrance.longitude];
  }
  const closestFrom = lookup.getClosestNetworkPt(fromLocation[0], fromLocation[1]);
  const closestTo = lookup.getClosestNetworkPt(toLocation[0], toLocation[1]);

  const result = finder.findPath(closestFrom, closestTo);
  const paths = result.geojsonPath.features.map(feature => {
    return feature.geometry.coordinates;
  });

  let finalPath = [fromLocation, closestFrom];
  paths.forEach(path => {
    finalPath.push(path[0]);
    finalPath.push(path[1]);
  });
  finalPath.push(closestTo);
  finalPath.push(toLocation);
  const finalPathFilter = [];
  finalPath.forEach(element => {
    const inSet = finalPathFilter.find(item => {
      return element[0] == item[0] && element[1] == item[1];
    });
    if (inSet) {
      return;
    } else {
      finalPathFilter.push(element);
    }
  });
  return finalPathFilter;
}
