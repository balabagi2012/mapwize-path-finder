import Api from "../api";
import fs from "fs";
import path from "path";
import PathFinder from "geojson-path-finder";
import placesJson from "../mock/places-mapwize";
import geojson from "../mock/routeGraph-geoJson";
import findBestPath from "./findPath";
export default class Utils {
  static findBestPath(fromPlaceId, toPlaceId) {
    return findBestPath(fromPlaceId, toPlaceId);
  }

  static async fetchRoutegraphJson() {
    const result = await Api.getRoutegraphs();
    fs.writeFileSync(
      path.resolve(__dirname, "../mock/routeGraph-mapwize.json"),
      JSON.stringify(result),
      "utf8"
    );
    return result;
  }

  static async fetchPlacesJson() {
    const result = await Api.getPlaces();
    fs.writeFileSync(
      path.resolve(__dirname, "../mock/places-mapwize.json"),
      JSON.stringify({ places: result }),
      "utf8"
    );
    return result;
  }

  static async transformMapwizeToGeoJson(source) {
    const { routes, nodes } = source;

    const routesWithNode = routes.map(route => {
      route.from = nodes.find(node => node.id === route.from);
      route.to = nodes.find(node => node.id === route.to);
      return route;
    });

    const routeFeature = routesWithNode.map(route => {
      const { lat: fromLat, lon: fromLon } = route.from;
      const { lat: toLat, lon: toLon } = route.to;
      return {
        type: "Feature",
        properties: {
          _id: route.id,
          _cost: 1
        },
        geometry: {
          type: "LineString",
          coordinates: [
            [fromLat, fromLon],
            [toLat, toLon]
          ]
        }
      };
    });

    const routeGeoJson = {
      type: "FeatureCollection",
      features: routeFeature
    };
    fs.writeFileSync(
      path.resolve(__dirname, "../mock/routeGraph-geoJson.json"),
      JSON.stringify(routeGeoJson),
      "utf8"
    );
    return routeGeoJson;
  }

  static findPath(fromPlaceId, toPlaceId) {
    const pathFinder = new PathFinder(geojson, { precision: 0.00002 });
    const fromPlace = placesJson.places.find(
      place => place._id === fromPlaceId
    );
    const toPlace = placesJson.places.find(place => place._id === toPlaceId);
    const fromPlaceEntrance = fromPlace.entrance
      ? fromPlace.entrance
      : fromPlace.marker;
    const toPlaceEntrance = toPlace.entrance
      ? toPlace.entrance
      : toPlace.marker;
    const startPoint = {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [fromPlaceEntrance.latitude, fromPlaceEntrance.longitude]
      }
    };
    const endPoint = {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [toPlaceEntrance.latitude, toPlaceEntrance.longitude]
      }
    };
    const pathResult = pathFinder.findPath(startPoint, endPoint);
    let result = [
      // [fromPlace.marker.latitude, fromPlace.marker.longitude],
      startPoint.geometry.coordinates
      // [toPlace.marker.latitude, toPlace.marker.longitude],
    ];
    if (pathResult) {
      result = [...result, ...pathResult.path];
    }
    result = [...result, endPoint.geometry.coordinates];
    console.log(result);
    return result;
  }
}
