import express from "express";
import Api from "../api";
import Utils from "../utils";
const router = express.Router();

router.get("/", async function (req, res, next) {
  const result = await Api.getRoutegraphs();
  res.json(result[0].routes.length);
});

router.post("/", async function (req, res, next) {
  const { from, to } = req.body;
  const result = await Utils.findBestPath(from, to);
  res.json(result);
});

export default router;
